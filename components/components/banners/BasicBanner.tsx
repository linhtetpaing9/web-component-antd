import React from 'react'
import { Carousel } from 'antd'
import BasicBannerPhotos from '../../../data/basic-banner-photos'
import { CMSBannerComponent } from './type'

const BasicBanner = ({ list, children }: CMSBannerComponent) => {
  if (children != null){
    return children(list)
  }
  return (
    <div className="banner-basic-home-slider">
      <Carousel>
        {
          list.map((banner, index) => (
            <div className="banner-slider" key={`home-banner-${index}`}>
              <div
                className="banner-img"
                style={{
                  backgroundImage: `url("${banner.url}")`,
                }} />
              <div className="banner-centered-text">
                <h1>{banner?.title || ''}</h1>
                <p>{banner?.description || ''}</p>
              </div>
            </div>
          ))
        }
      </Carousel>
    </div>
  )
}

// type IsFunction<T> = T extends (...args: any[]) => any ? T : never
// const isFunction = <T extends {}> (value: T): value is IsFunction<T> => typeof value  === 'function'

{/* <BasicBanner>
  {(list) =>
    <div className="banner-basic-home-slider">
      <Carousel>
        {
          list.map((banner, index) => (
            <div className="banner-slider" key={`home-banner-${index}`}>
              <div
                className="banner-img"
                style={{
                  backgroundImage: `url("${banner.url}")`,
                }} />
              <div className="banner-centered-text">
                <h1>{banner?.title || ''}</h1>
                <p>{banner?.description || ''}</p>
              </div>
            </div>
          ))
        }
      </Carousel>
    </div>
  }
</BasicBanner> */}

BasicBanner.defaultProps = {
  list: BasicBannerPhotos
}

export default BasicBanner