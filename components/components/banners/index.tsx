import BasicBanner from './BasicBanner'
import BannerSlider from './BannerSlider'
import { PicCenterOutlined } from '@ant-design/icons';

export default {
  type: "BannerRepo",
  icon: <PicCenterOutlined />,
  render: {
    BasicBanner,
    BannerSlider
  }
}