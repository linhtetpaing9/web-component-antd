export interface CMSBannerComponent {
  defaultProps?: { list: BannerItem[] };
  children?: (list: BannerItem[]) => any;
  list: BannerItem[];
}

interface BannerItem {
  url: string,
  title: string,
  description?: string
}
