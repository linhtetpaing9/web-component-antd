import { Button, Col, Row } from "antd"
import React from "react"
import TourCardItem from "../../../data/tour-card-item"

const HorizontalTourCard1 = ({ tour = TourCardItem }) => {
  return (
    <div className="vacation-card">
      <Row>
        <Col md={12}>
          <div className="vacation-img">
            <img src={tour.url} />
          </div>
        </Col>
        <Col md={12}>
          <div className="vacation-content">
            <h4>{tour.title}</h4>
            <h6>{tour.desc}</h6>
            <div className="vacation-divider" />
            <div className="vacation-footer">
              <div className="fs-17">
                From:<br />
                <span className="black"><strong>S${tour.price} </strong></span>
              </div>
              <Button className="vacation-btn">
                Explore
              </Button>
              {/* <button className="vacation-btn">Explore</button> */}
            </div>
          </div>
        </Col>
      </Row>
    </div>
  )
}
export default HorizontalTourCard1