import React, { CSSProperties, ReactChild } from 'react';
import TourCardItem from '../../../data/tour-card-item';
import Image from 'next/image'

export interface FixedAscpectRatioContext {
  ratio?: string,
  children?: ReactChild,
  imageUrl?: string,
  className?: string,
  style?: CSSProperties
}

export const FixedAspectRatio = ({
  ratio = "1:1",
  children,
  imageUrl,
  className,
  style = {}
}: FixedAscpectRatioContext) => {

  const [width, height] = ratio.split(':')
  const percentage = (parseInt(height) / parseInt(width)) * 100

  if (imageUrl) style.backgroundImage = `url(${imageUrl})`

  return (
    <div className="fixed-aspect-ratio" style={{ paddingTop: `${percentage}%` }}>
      <div className={`fixed-aspect-ratio-background ${className}`} style={style}>
        {children}
      </div>
    </div>
  )
}

const TourCard1 = ({ item = TourCardItem }) => {
  return (
    <div className="tour-card-1" style={{ maxWidth: 340 }}>
      <div className="tour-card-1-img">
        <FixedAspectRatio style={{ borderRadius: 5 }} ratio="4:3" imageUrl={item.url} />
        {/* {sgLabel && <img className="position-left" src="/img/single-label.png" />}
        {dualLabel && <img className="dual-position-left" src="/img/dual-label.png" />}
        {trioLabel && <img className="position-left" src="/img/trio-label.png" />}
        {featured && <img className="position-right" src="/img/yellow-tagged.png" />}
        {srvEligible && <Tag className="srv-btn">Eligible for SingapoRediscovers Voucher</Tag>} */}
      </div>
      <div className="tour-card-1-content">
        <span>
          <Image
            src="/tour-card/location-tagged.png"
            width={12}
            height={16}
            alt="location"
          />
          {item.location}
        </span>
        <h4>{item.title}</h4>
        {
          item.desc.length > 250 ?
            <p>{item.desc.substring(0, 250).concat('...')}</p>
            :
            <p>{item.desc}</p>
        }
      </div>
      <div className="tour-card-1-divider" />
      <div className="tour-card-1-footer">
        <div className="tour-card-1-price">
          <span>S$ {item.price} </span>
          /人起
        </div>
        <button className="tour-card-1-btn">查看 <Image
            src="/tour-card/right-arrow.png"
            width={14}
            height={10}
            alt="right arrow"
          />
        </button>
      </div>
    </div>
  )
}
export default TourCard1