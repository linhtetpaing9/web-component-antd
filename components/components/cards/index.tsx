import TourCard1 from './TourCard1';
import HorizontalTourCard1 from './HorizontalTourCard1';
import { PicCenterOutlined } from '@ant-design/icons';

export default {
  type: "CardRepo",
  icon: <PicCenterOutlined />,
  render: {
    TourCard1,
    HorizontalTourCard1
  }
}