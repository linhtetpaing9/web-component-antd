import NavigationRepo from './navigations';
import BannerRepo from './banners';
import CardRepo from './cards';

export default {
  NavigationRepo,
  BannerRepo,
  CardRepo
}