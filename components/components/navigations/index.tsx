import BasicNav from './BasicNav'
import MenuNav from './MenuNav'
import { MenuOutlined } from '@ant-design/icons';

export default {
  type: "NavigationRepo",
  icon: <MenuOutlined />,
  render: {
    BasicNav,
    MenuNav
  }
}