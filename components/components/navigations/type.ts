export interface CMSMenuComponent {
  windowSize: { width: Number, height: Number };
  menus: (SubMenuItem & MenuItem)[];
  cart?: boolean;
  openCart?: () => any;
  defaultMobileSize?: Number;
  // activeClassName?: string,
  className?: string;
}

interface MenuItem { 
  name: string;
  sequence?: Number;
  link: string;
}
interface SubMenuItem {
  subMenuItems: MenuItem[];
  showAsColumns: boolean;
  itemCountPerColumn: Number;
}
