import React, { createContext, useEffect, useState } from "react";
import { Layout } from "antd";
import Head from "next/head";
import MetaTag from "./MetaTag";

const { Header, Content } = Layout;

export const LayoutContext: any = createContext({});

const BasicLayout = ({ children }: any) => {
  const [windowSize, setWindowSize] = useState<any>({});

  useEffect(() => {
    checkWindowSize();
    window.addEventListener("resize", checkWindowSize);
  }, []);

  const checkWindowSize = () => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  return (
    <LayoutContext.Provider
      value={{
        windowSize,
      }}
    >
      <Layout>
        <Header>
          <Head>
            <MetaTag />
          </Head>
        </Header>
        <Content>
          {children}
        </Content>
      </Layout>
    </LayoutContext.Provider>
  );
};
export default BasicLayout;
