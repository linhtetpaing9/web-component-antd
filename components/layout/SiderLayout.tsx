import React, { useState } from 'react';
import { Layout, Menu } from 'antd';
import Components from '../components'
import Link from 'next/link';
import styles from '../../styles/SiderLayout.module.scss';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const SiderLayout = ({ children }: any) => {
  const [collapsed, setCollapsed] = useState(false);
  const onCollapse = (collapsed: any) => {
    setCollapsed(collapsed)
  };

  const components = Object.entries(Components)

  return (
    <Layout className={styles.whole_layout}>
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
        <div className={styles.logo} />
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          {
            components.map(([name, { render, icon }]) => {
              const subComponents = Object.entries(render)
              return (
                <SubMenu key={name} icon={icon} title={name}>
                  {
                    subComponents.map(([subName]) => (
                      <Menu.Item key={subName}>
                        <Link
                          href={{
                            pathname: '/',
                            query: { parent: name, name: subName }
                          }}
                        >
                          <a>
                            {subName}
                          </a>
                        </Link>
                      </Menu.Item>
                    ))
                  }
                </SubMenu>
              )
            })
          }
        </Menu>
      </Sider>
      <Layout className={styles.site_layout}>
        <Header className={styles.site_layout_header} />
        <Content className={styles.site_content}>
          <div className={styles.site_layout_background}>
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>
    </Layout>
  )
}

export default SiderLayout;