const BasicBannerPhotos = [
  {
    url: "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/planettravel.com.sg%2Fhome%2Fbanner-slider%2Fbanner-bg.jpg?alt=media&token=b770dfa1-1fc1-4c56-857d-5ba8547fccea",
    title: "Hello Banner",
  },
  {
    url: "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/planettravel.com.sg%2Fhome%2Fbanner-slider%2Fbanner-bg.jpg?alt=media&token=b770dfa1-1fc1-4c56-857d-5ba8547fccea",
    title: "First Banner",
  },
  {
    url: "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/planettravel.com.sg%2Fhome%2Fbanner-slider%2Fbanner-bg.jpg?alt=media&token=b770dfa1-1fc1-4c56-857d-5ba8547fccea",
    title: "Second Banner",
  },
];

export default BasicBannerPhotos