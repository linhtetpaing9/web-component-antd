const MenuList = [
  {
    sequence: 1,
    name: "HOME",
    link: "/",
  },
  {
    name: "CORPORATE",
    sequence: 2,
    link: "/corporate-travel",
  },
  {
    name: "M.I.C.E",
    sequence: 3,
    link: "/mice",
  },
  {
    subMenuItems: [
      {
        name: "Australasia",
        link: "/australasia",
      },
      {
        name: "Australasia",
        link: "/australasia",
      },
    ],
    showAsColumns: false,
    link: "/",
    itemCountPerColumn: 3,
    name: "LEISURE",
    sequence: 4,
  },
  {
    sequence: 5,
    name: "ONLINE BOOKING",
    link: "/",
  },
  {
    sequence: 6,
    link: "/about",
    name: "ABOUT",
  },
  {
    sequence: 7,
    name: "CONTACT",
    link: "/",
  },
];

export default MenuList;