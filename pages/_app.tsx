import App from 'next/app'
import '../styles/styles.scss'

import type { AppProps } from 'next/app'
import { useEffect } from 'react'
import Link from "next/link";
import SiderLayout from '../components/layout/SiderLayout'
import { setupUserback } from "travelcloud-antd";
import { Button, Result } from 'antd'
import React from 'react';

interface ObjectLiteral {
  [key: string]: any;
}

type ServerSideProps = {
  notFound: boolean,
  content: ObjectLiteral,
} & AppProps

function MyApp({
  Component,
  pageProps,
  notFound,
}: ServerSideProps) {

  useEffect(() => {
    setupUserback();
  }, [])

  const backToHome = (
    <Link href="/">
      <a>
        <div className="flex-button">
          <Button type="primary" size="large">
            Back Home
        </Button>
        </div>
      </a>
    </Link>
  );

  return (
    <SiderLayout>
      {notFound ? (
        <Result
          status={500}
          title="Error"
          subTitle="Sorry, something went wrong."
          extra={backToHome}
        />
      ) : pageProps["statusCode"] ? (
        <Result
          status={pageProps["statusCode"]}
          title={`Error: ${pageProps["statusCode"]}`}
          subTitle="Sorry, something went wrong."
          extra={backToHome}
        />
      ) : (
        <Component {...pageProps} />
      )}
    </SiderLayout>
  )
}

MyApp.getInitialProps = async (appContext: any) => {
  const appProps = await App.getInitialProps(appContext);
  try {
    return { ...appProps }
  } catch (error) {
    return {
      notFound: true
    }
  }
}

export default MyApp
