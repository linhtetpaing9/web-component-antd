import { getContent } from "./web-component";
import data from './data.json'

function mFetch(data) {
  return Promise.resolve(data)
}

test("Testing getContent()", async () => {
  const content = await mFetch(data)
  expect(getContent(content).blockId("header-extra").getCode()).toBe('header-extra');
});
