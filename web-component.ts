import config from "./customize/config";

export function sum(a: number, b: number): number {
  return a + b;
}

export function getContent(content = {}) {
  const entryContent = Object.entries(content);
  var state = [];
  return {
    log() {
      console.log(this);
    },
    code(code) {
      state.push(
        entryContent.find(([, value]: any) => value?.code == code) || []
      );
      this.uniqueArray();
      this.get();
      return this;
    },
    blockId(blockId) {
      state.push(entryContent.find(([code]) => code === blockId) || []);
      this.uniqueArray();
      this.get();
      return this;
    },
    blockIds(blockIds) {
      const validatedBlockIds = blockIds.map((blockId) => blockId);
      state.push(
        ...(entryContent.filter(
          ([code]) => validatedBlockIds.indexOf(code) > -1
        ) || [])
      );
      this.uniqueArray();
      this.get();
      return this;
    },
    uniqueArray() {
      state = state.filter(
        ([code], index, self) =>
          self.map(([code]) => code).indexOf(code) === index
      );
    },
    tag(tag) {
      state.push(
        ...(entryContent.filter(
          ([, content]: any) => content?.tags?.indexOf(tag) > -1
        ) || [])
      );
      this.uniqueArray();
      return this;
    },
    // multiple tags later
    sortLTH(item) {
      state = state.sort(([, a], [, b]) => {
        return parseInt(a?.[item] || 0) - parseInt(b?.[item] || 1);
      });
      return this;
    },
    sortHTL(item) {
      state = state.sort(
        ([, a], [, b]) => parseInt(b?.[item] || 0) - parseInt(a?.[item] || 1)
      );
      return this;
    },
    getCode() {
      //need refactor
      if (state.length > 1) {
        return state.map(([code]) => code);
      }
      const [code] = state[0];
      return code;
    },
    getValue() {
      if (state.length > 1) {
        return state.map(([, value]) => value);
      }
      const [, value] = state[0];
      return value;
    },
    get() {
      if (state === undefined || state.length == 0) {
        return entryContent[0];
      }

      if (state.length > 1) {
        return state;
      }
      const value = state[0];
      return value;
    },
    all() {
      if (state === undefined || state.length == 0) {
        return entryContent[0];
      }
      return state;
    },
  };
}

export function resolvePath(...args: string[]): string {
  return args
    .map((token: any) => (token.slice(0, 1) === "/" ? token.slice(1) : token))
    .map((token: any) => (token.slice(-1) === "/" ? token.slice(0, -1) : token))
    .join("/");
}

export async function getDocument(
  path: string,
  domain: string = config.domain
) {
  let url = domain != null ? resolvePath(domain, path) : path;
  console.log("url", url);
  console.log({ url });
  let resp = await fetch(
    `https://asia-east2-headless-cms-292305.cloudfunctions.net/pageDetails?url=${encodeURIComponent(
      url
    )}`
  );
  return await resp.json();
}
